Liebe GENOSS:INNEN!

hiermit laden wir euch herzlich zur Konferenz 2023 ein. Sie findet statt

   am Donnerstag, den 04.05.2023 17 Uhr 
   bis Sonntag, den 07.05.2023 10 Uhr
   im [Tagungshaus Niederkaufungen](https://www.tagungshaus-niederkaufungen.de/informationen/), Kirchweg 1, 34260 Kaufungen**

Wir bitten dich und euch um eine verlässliche Zusage bis **28.02.2023**. 

## Programmvorschlag
Samstag Fokus Kollektiv:
* Rupay Dahm, Rechtsanwalt für Kollektivbetriebe über "tba" (Samstag)
* Sinje (Samstag)

Freitag Fokus IT:
* Henning oder Niels: "IT-Kollektiv: was bisher geschah" 
* Robert: "tech-coops: Was waren unsere Ideen im DragonDreaming? Wo stehen wir jetzt?"
* Du!, denn die Konferenz ist eher eine Unkonferenz (a.k.a. OpenSpace): Wir bringen uns ein zu den Themen 
   * "Solidarität - was und wie machen wir das? Wir weit können wir gehen. Wie weit wollen wir gehen?"
   * "IT-Kollektive-gemeinsame Themen (ITKGT)": Infrastructure Dreaming, Aquise, Marketing, Verdienst / Finanzkontrolling, 
   * Dein Beitrag zu einem spannenden Thema
   * Party, Chillen und Wandern
   
Costa quanta? 500 EUR all in.


